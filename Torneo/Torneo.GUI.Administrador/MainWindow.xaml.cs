﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Torneo.BIZ;
using Torneo.COMMON.Entidades;
using Torneo.COMMON.Interfaz;
using Torneo.DAL;

namespace Torneo.GUI.Administrador
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        enum accion
        {
            Nuevo,
            Editar
        }
        IManejadorDeporte ManejadorDeporte;
        IManejadorEquipo ManejadorEquipo;
        IManejadorUsuario ManejadorUsuario;
        //IManejadorTorneo ManejadorTorneo;
        IManejadorTorneoFin4 ManejadorTorneoFin;

        accion accionDeporte;
        accion accionEquipo;
        accion accionUsuario;
        //accion accionTorneo;
        accion accionPartido;

        List<EquiposTorneo> equipostorneo;
        List<EquiposTorneo> impar;
        string equipo1="", equipo2="";
        public MainWindow()
        {
            InitializeComponent();
            ManejadorDeporte = new ManejadorDeporte(new RepositorioDeporte());
            ManejadorEquipo = new ManejadorEquipo(new RepositorioEquipo());
            ManejadorUsuario = new ManejadorUsuario(new RepositorioUsuario());
            //ManejadorTorneo = new ManejadorTorneo(new RepositorioTorneo());
            ManejadorTorneoFin = new ManejadorTorneoFin(new RepositorioTorneoFin());

            BotonesDeporteEdicion(false);
            LimpiarCamposDeportes();
            ActualizarTablaDeportes();

            BotonesEquipoEdicion(false);
            LimpiarCamposEquipos();
            ActualizarTablaEquipos();
            ActualizarEquipoDeporte();

            BotonesUsuarioEdicion(false);
            LimpiarCamposUsuario();
            ActualizarTablaUsuario();

            //BotonesTorneoEdicion(false);
            //LimpiarCamposTorneo();
            //ActualizarTablaTorneo();
            //ActualizarTorneoTipo();

            BotonesPartidoEdicion(false);
            LimpiarCamposPartido();
            ActualizarTablaTorneosFin();

            equipostorneo = new List<EquiposTorneo>();
            impar = new List<EquiposTorneo>();
        }

        private void LimpiarCamposPartido()
        {
            cmbDeporteTor.Text = "";
            clcFachaTor.Text = "";
        }

        private void BotonesPartidoEdicion(bool value)
        {
            btnPartidoCancelar.IsEnabled = value;
            btnPartidoEditar.IsEnabled = !value;
            btnPartidoEliminar.IsEnabled = !value;
            btnPartidoGuardar.IsEnabled = value;
            btnPartidoNuevo.IsEnabled = !value;
            cmbDeporteTor.IsEnabled = value;
            clcFachaTor.IsEnabled = value;
        }

        //private void ActualizarTorneoTipo()
        //{
        //    cmbTipoTorneo.ItemsSource = null;
        //    cmbTipoTorneo.ItemsSource = ManejadorDeporte.Listar;
        //}

        //private void ActualizarTablaTorneo()
        //{
        //    dtgTorneos.ItemsSource = null;
        //    dtgTorneos.ItemsSource = ManejadorTorneo.Listar;
        //}

        //private void LimpiarCamposTorneo()
        //{
        //    cmbTipoTorneo.Text = "";
        //    dtpFechaTorneo.Text = "";
        //    txbTorneoId.Text = "";
        //}

        //private void BotonesTorneoEdicion(bool value)
        //{
        //    btnTorneoCancelar.IsEnabled = value;
        //    btnTorneoEditar.IsEnabled = !value;
        //    btnTorneoEliminar.IsEnabled = !value;
        //    btnTorneoGuardar.IsEnabled = value;
        //    btnTorneoNuevo.IsEnabled = !value;
        //    dtpFechaTorneo.IsEnabled = value;
        //    cmbTipoTorneo.IsEnabled = value;
        //}

        private void ActualizarTablaUsuario()
        {
            dtgUsuarios.ItemsSource = null;
            dtgUsuarios.ItemsSource = ManejadorUsuario.Listar;
        }

        private void LimpiarCamposUsuario()
        {
            txbNombreUsuario.Clear();
            txbContraseniaUsuario.Clear();
            txbUsuarioId.Text = "";
        }

        private void BotonesUsuarioEdicion(bool value)
        {
            btnUsuarioCancelar.IsEnabled = value;
            btnUsuarioEditar.IsEnabled = !value;
            btnUsuarioEliminar.IsEnabled = !value;
            btnUsuarioGuardar.IsEnabled = value;
            btnUsuarioNuevo.IsEnabled = !value;
            txbNombreUsuario.IsEnabled = value;
            txbContraseniaUsuario.IsEnabled = value;
        }

        private void ActualizarEquipoDeporte()
        {
            cmbEquipoDeporte.ItemsSource = null;
            cmbEquipoDeporte.ItemsSource = ManejadorDeporte.Listar;

            cmbDeporteTor.ItemsSource = null;
            cmbDeporteTor.ItemsSource = ManejadorDeporte.Listar;
        }

        private void ActualizarTablaEquipos()
        {
            dtgEquipos.ItemsSource = null;
            dtgEquipos.ItemsSource = ManejadorEquipo.Listar;
        }

        private void LimpiarCamposEquipos()
        {
            txbNombreEquipo.Clear();
            txbEquipoId.Text = "";
            cmbEquipoDeporte.Text = "";
        }

        private void BotonesEquipoEdicion(bool value)
        {
            btnEquipoCancelar.IsEnabled = value;
            btnEquipoEditar.IsEnabled = !value;
            btnEquipoEliminar.IsEnabled = !value;
            btnEquipoGuardar.IsEnabled = value;
            btnEquipoNuevo.IsEnabled = !value;
            txbNombreEquipo.IsEnabled = value;
            cmbEquipoDeporte.IsEnabled = value;
        }

        private void ActualizarTablaDeportes()
        {
            dtgDeportes.ItemsSource = null;
            dtgDeportes.ItemsSource = ManejadorDeporte.Listar;
        }

        private void LimpiarCamposDeportes()
        {
            txbNombreDeporte.Clear();
            txbDeporteId.Text = "";
        }

        private void BotonesDeporteEdicion(bool value)
        {
            btnDeporteCancelar.IsEnabled = value;
            btnDeporteEditar.IsEnabled = !value;
            btnDeporteEliminar.IsEnabled = !value;
            btnDeporteGuardar.IsEnabled = value;
            btnDeporteNuevo.IsEnabled = !value;
            txbNombreDeporte.IsEnabled = value;
        }

        private void btnDeporteNuevo_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDeportes();
            BotonesDeporteEdicion(true);
            accionDeporte = accion.Nuevo;
        }

        private void btnDeporteEditar_Click(object sender, RoutedEventArgs e)
        {
            Deporte dep = dtgDeportes.SelectedItem as Deporte;
            if (dep != null)
            {
                txbDeporteId.Text = dep.Id;
                txbNombreDeporte.Text = dep.NombreDeporte;
                accionDeporte = accion.Editar;
                BotonesDeporteEdicion(true);
            }
        }

        private void btnDeporteGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (accionDeporte == accion.Nuevo)
            {
                Deporte dep = new Deporte()
                {
                    NombreDeporte = txbNombreDeporte.Text,
                };
                if (ManejadorDeporte.Agregar(dep))
                {
                    MessageBox.Show("Deporte agregado correctamente", "inventarios", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarCamposDeportes();
                    ActualizarTablaDeportes();
                    ActualizarEquipoDeporte();
                    BotonesDeporteEdicion(false);
                }
                else
                {
                    MessageBox.Show("El Deporte no pudo ser agregado", "inventarios", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                Deporte dep = dtgDeportes.SelectedItem as Deporte;
                dep.NombreDeporte = txbNombreDeporte.Text;
                if (ManejadorDeporte.Modificar(dep))
                {
                    MessageBox.Show("Deporte modificado correctamente", "inventarios", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarCamposDeportes();
                    ActualizarTablaDeportes();
                    ActualizarEquipoDeporte();
                    BotonesDeporteEdicion(false);
                }
                else
                {
                    MessageBox.Show("El Deporte no pudo ser actualizado", "inventarios", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void btnDeporteCancelar_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDeportes();
            BotonesDeporteEdicion(false);
        }

        private void btnDeporteEliminar_Click(object sender, RoutedEventArgs e)
        {
            Deporte dep = dtgDeportes.SelectedItem as Deporte;
            if (dep != null)
            {
                if (MessageBox.Show("Realmente desea eliminar este Deporte?", "Inventarios", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (ManejadorDeporte.Eliminar(dep.Id))
                    {
                        MessageBox.Show("Deporte eliminado", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Information);
                        ActualizarTablaDeportes();
                        ActualizarEquipoDeporte();
                    }
                    else
                    {
                        MessageBox.Show("No se pudo eliminar el Deporte", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
        }

        private void btnEquipoNuevo_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposEquipos();
            BotonesEquipoEdicion(true);
            accionEquipo = accion.Nuevo;
        }

        private void btnEquipoEditar_Click(object sender, RoutedEventArgs e)
        {
            Equipo equ = dtgEquipos.SelectedItem as Equipo;
            if (equ != null)
            {
                txbEquipoId.Text = equ.Id;
                txbNombreEquipo.Text = equ.NombreEquipo;
                cmbEquipoDeporte.Text = equ.TipoDeporte;
                accionEquipo = accion.Editar;
                BotonesEquipoEdicion(true);
            }
        }

        private void btnEquipoGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (accionEquipo == accion.Nuevo)
            {
                Equipo equ = new Equipo()
                {
                    NombreEquipo = txbNombreEquipo.Text,
                    TipoDeporte = cmbEquipoDeporte.Text
                };
                if (ManejadorEquipo.Agregar(equ))
                {
                    MessageBox.Show("Equipo agregado correctamente", "inventarios", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarCamposEquipos();
                    ActualizarEquipoDeporte();
                    ActualizarTablaEquipos();
                    BotonesEquipoEdicion(false);
                }
                else
                {
                    MessageBox.Show("El Equipo no pudo ser agregado", "inventarios", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                Equipo equ = dtgEquipos.SelectedItem as Equipo;
                equ.NombreEquipo = txbNombreEquipo.Text;
                equ.TipoDeporte = cmbEquipoDeporte.Text;
                if (ManejadorEquipo.Modificar(equ))
                {
                    MessageBox.Show("Equipo modificado correctamente", "inventarios", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarCamposEquipos();
                    ActualizarEquipoDeporte();
                    ActualizarTablaEquipos();
                    BotonesEquipoEdicion(false);
                }
                else
                {
                    MessageBox.Show("El Equipo no pudo ser actualizado", "inventarios", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void btnEquipoCancelar_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposEquipos();
            BotonesEquipoEdicion(false);
        }

        private void btnEquipoEliminar_Click(object sender, RoutedEventArgs e)
        {
            Equipo equ = dtgEquipos.SelectedItem as Equipo;
            if (equ != null)
            {
                if (MessageBox.Show("Realmente desea eliminar este Equipo?", "Inventarios", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (ManejadorEquipo.Eliminar(equ.Id))
                    {
                        MessageBox.Show("Equipo eliminado", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Information);
                        ActualizarEquipoDeporte();
                        ActualizarTablaEquipos();
                    }
                    else
                    {
                        MessageBox.Show("No se pudo eliminar el Equipo", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
        }

        private void btnUsuarioNuevo_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposUsuario();
            BotonesUsuarioEdicion(true);
            accionUsuario = accion.Nuevo;
        }

        private void btnUsuarioEditar_Click(object sender, RoutedEventArgs e)
        {
            Usuario usu = dtgUsuarios.SelectedItem as Usuario;
            if (usu != null)
            {
                txbUsuarioId.Text = usu.Id;
                txbNombreUsuario.Text = usu.NombreUsuario;
                txbContraseniaUsuario.Text = usu.Contrasenia;
                accionUsuario = accion.Editar;
                BotonesUsuarioEdicion(true);
            }
        }

        private void btnUsuarioGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (accionUsuario == accion.Nuevo)
            {
                Usuario usu = new Usuario()
                {
                    NombreUsuario = txbNombreUsuario.Text,
                    Contrasenia = txbContraseniaUsuario.Text
                };
                if (ManejadorUsuario.Agregar(usu))
                {
                    MessageBox.Show("Usuario agregado correctamente", "inventarios", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarCamposUsuario();
                    ActualizarTablaUsuario();
                    BotonesUsuarioEdicion(false);
                }
                else
                {
                    MessageBox.Show("El Usuario no pudo ser agregado", "inventarios", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                Usuario usu = dtgUsuarios.SelectedItem as Usuario;
                usu.NombreUsuario = txbNombreUsuario.Text;
                usu.Contrasenia = txbContraseniaUsuario.Text;
                if (ManejadorUsuario.Modificar(usu))
                {
                    MessageBox.Show("Usuario modificado correctamente", "inventarios", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarCamposUsuario();
                    ActualizarTablaUsuario();
                    BotonesUsuarioEdicion(false);
                }
                else
                {
                    MessageBox.Show("El Usuario no pudo ser actualizado", "inventarios", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void btnUsuarioCancelar_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposUsuario();
            BotonesUsuarioEdicion(false);
        }

        private void btnUsuarioEliminar_Click(object sender, RoutedEventArgs e)
        {
            Usuario usu = dtgUsuarios.SelectedItem as Usuario;
            if (usu != null)
            {
                if (MessageBox.Show("Realmente desea eliminar este Usuario?", "Inventarios", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (ManejadorUsuario.Eliminar(usu.Id))
                    {
                        MessageBox.Show("Usuario eliminado", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Information);
                        ActualizarTablaUsuario();
                    }
                    else
                    {
                        MessageBox.Show("No se pudo eliminar el Usuario", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
        }

        //private void btnTorneoNuevo_Click(object sender, RoutedEventArgs e)
        //{
        //    LimpiarCamposTorneo();
        //    BotonesTorneoEdicion(true);
        //    accionTorneo = accion.Nuevo;
        //}

        //private void btnTorneoEditar_Click(object sender, RoutedEventArgs e)
        //{
        //    Torneos tor = dtgTorneos.SelectedItem as Torneos;
        //    if (tor != null)
        //    {
        //        txbTorneoId.Text = tor.Id;
        //        dtpFechaTorneo.SelectedDate = tor.FechaProgramada;
        //        cmbTipoTorneo.Text = tor.TipoTorneo;
        //        accionTorneo = accion.Editar;
        //        BotonesTorneoEdicion(true);
        //    }
        //}

        //private void btnTorneoGuardar_Click(object sender, RoutedEventArgs e)
        //{
        //    if (accionEquipo == accion.Nuevo)
        //    {
        //        Torneos tor = new Torneos()
        //        {
        //            TipoTorneo = cmbTipoTorneo.Text,
        //            FechaProgramada = DateTime.Parse(dtpFechaTorneo.ToString())
        //    };
        //        if (ManejadorTorneo.Agregar(tor))
        //        {
        //            MessageBox.Show("Torneo agregado correctamente", "inventarios", MessageBoxButton.OK, MessageBoxImage.Information);
        //            LimpiarCamposTorneo();
        //            ActualizarTorneoTipo();
        //            ActualizarTablaTorneo();
        //            BotonesTorneoEdicion(false);
        //        }
        //        else
        //        {
        //            MessageBox.Show("El Torneo no pudo ser agregado", "inventarios", MessageBoxButton.OK, MessageBoxImage.Error);
        //        }
        //    }
        //    else
        //    {
        //        Torneos tor = dtgTorneos.SelectedItem as Torneos;
        //        tor.TipoTorneo = cmbTipoTorneo.Text;
        //        tor.FechaProgramada = DateTime.Parse(dtpFechaTorneo.ToString());
        //        if (ManejadorTorneo.Modificar(tor))
        //        {
        //            MessageBox.Show("Torneo modificado correctamente", "inventarios", MessageBoxButton.OK, MessageBoxImage.Information);
        //            LimpiarCamposTorneo();
        //            ActualizarTorneoTipo();
        //            ActualizarTablaTorneo();
        //            BotonesTorneoEdicion(false);
        //        }
        //        else
        //        {
        //            MessageBox.Show("El Torneo no pudo ser actualizado", "inventarios", MessageBoxButton.OK, MessageBoxImage.Error);
        //        }
        //    }
        //}

        //private void btnTorneoCancelar_Click(object sender, RoutedEventArgs e)
        //{
        //    LimpiarCamposTorneo();
        //    BotonesTorneoEdicion(false);
        //}

        //private void btnTorneoEliminar_Click(object sender, RoutedEventArgs e)
        //{
        //    Torneos tor = dtgTorneos.SelectedItem as Torneos;
        //    if (tor != null)
        //    {
        //        if (MessageBox.Show("Realmente desea eliminar este Torneo?", "Inventarios", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
        //        {
        //            if (ManejadorTorneo.Eliminar(tor.Id))
        //            {
        //                MessageBox.Show("Torneo eliminado", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Information);
        //                ActualizarTorneoTipo();
        //                ActualizarTablaTorneo();
        //            }
        //            else
        //            {
        //                MessageBox.Show("No se pudo eliminar el Torneo", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Information);
        //            }
        //        }
        //    }
        //}

        private void ActualizarTablaTorneosFin()
        {
            dtgtorneofin.ItemsSource = null;
            dtgtorneofin.ItemsSource = ManejadorTorneoFin.Listar;
        }

        private void PrimerEquipo() {
           
            int contador = 0;
            EquiposTorneo c = new EquiposTorneo();
            foreach (var item in equipostorneo)
            {
                contador++;
                if (contador == 1) {
                   
                    equipo1 = item.Nombre;
                    equipostorneo.Remove(item);
                    break;
                }
            }
        }
        private void SegundoEquipo() {
            Random a = new Random();
            int b = a.Next(1, equipostorneo.Count);
            int contador = 0;
            EquiposTorneo c = new EquiposTorneo();
            foreach (var item in equipostorneo)
            {
                contador++;
                if (contador == b)
                {
                    equipo2 = item.Nombre;
                    equipostorneo.Remove(item);
                    break;
                }
            }
        }
        private void TercerEquipo() {
            Random a = new Random();
            int b = a.Next(1, impar.Count);
            int contador = 0;
            foreach (var item in impar)
            {
                contador++;
                if (contador == b)
                {
                    EquiposTorneo c = new EquiposTorneo();
                    c.Nombre = item.Nombre;
                    impar.Add(c);
                    equipo1 = item.Nombre;
                    equipostorneo.Remove(item);
                }
            }
        }

        private void AgregarATorneo() {
            PrimerEquipo();
            SegundoEquipo();
            TorneosFin a = new TorneosFin()
            {
                Fecha = clcFachaTor.Text,
                EquipoUno= equipo1,
                EquipoDos= equipo2,
                EquipoDosMarcador= 0, 
                EquipoUnoMarcador=0,
            };
            ManejadorTorneoFin.Agregar(a);
        }

        private void Editar_Click(object sender, RoutedEventArgs e)
        {
            if (accionPartido == accion.Editar) {
                TorneosFin a = dtgtorneofin.SelectedItem as TorneosFin;
                a.EquipoUno = Equipo1.Text;
                a.EquipoDos = Equipo2.Text;
                if (int.Parse(Marcador1.Text)> int.Parse(Marcador2.Text)) {
                    a.EquipoUnoMarcador = 3;
                    a.EquipoDosMarcador = 1;
                }
                if (int.Parse(Marcador1.Text) < int.Parse(Marcador2.Text)) {
                    a.EquipoUnoMarcador = 1;
                    a.EquipoDosMarcador = 3;
                }
                if (int.Parse(Marcador1.Text) == int.Parse(Marcador2.Text)) {
                    a.EquipoUnoMarcador = 2;
                    a.EquipoDosMarcador =2;
                }              
                ManejadorTorneoFin.Modificar(a);
            }
            ActualizarTablaTorneosFin();
        }

        private void dtgtorneofin_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (dtgtorneofin.SelectedItem!=null)
            {
                TorneosFin a = dtgtorneofin.SelectedItem as TorneosFin;
                Equipo1.Text = a.EquipoUno;
                Equipo2.Text = a.EquipoDos;
                Marcador1.Text = a.EquipoUnoMarcador.ToString();
                Marcador2.Text = a.EquipoDosMarcador.ToString();
                accionPartido = accion.Editar;
            }
        }

        private void btnPartidoGuardar_Click(object sender, RoutedEventArgs e)
        {
            equipostorneo = new List<EquiposTorneo>();
            if (string.IsNullOrEmpty(cmbDeporteTor.Text) || string.IsNullOrEmpty(clcFachaTor.Text))
            {
                MessageBox.Show("Favor de llenar el campo deporte o fecha", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            int contador = 0;
            foreach (var item in ManejadorEquipo.Listar)
            {
                if (item.TipoDeporte == cmbDeporteTor.Text)
                {
                    contador++;
                }
            }
            if (contador <= 1)
            {
                MessageBox.Show("No se puede generar ningun Partido, no cuenta con demasiados equipos", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            else
            {
                foreach (var item in ManejadorEquipo.Listar)
                {
                    if (item.TipoDeporte == cmbDeporteTor.Text)
                    {
                        EquiposTorneo a = new EquiposTorneo();
                        a.Nombre = item.NombreEquipo;
                        // a.Puntuacion = 0;
                        equipostorneo.Add(a);
                    }
                }

                if (contador % 2 == 0)
                {
                    while (((equipostorneo.Count) / 2) > 0)
                    {
                        AgregarATorneo();
                    }
                }
                else
                {
                    while (((equipostorneo.Count) / 2) > 0)
                    {
                        AgregarATorneo();
                    }
                    AgregarATorneoImp();
                }
            }
            LimpiarCamposPartido();
            ActualizarTablaTorneosFin();
            BotonesPartidoEdicion(false);
        }

        private void btnPartidoCancelar_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposPartido();
            BotonesPartidoEdicion(false);
        }

        private void btnPartidoEliminar_Click(object sender, RoutedEventArgs e)
        {
            TorneosFin tor = dtgtorneofin.SelectedItem as TorneosFin;
            if (tor != null)
            {
                if (MessageBox.Show("Realmente desea eliminar este Partido?", "Inventarios", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (ManejadorTorneoFin.Eliminar(tor.Id))
                    {
                        MessageBox.Show("Partido eliminado", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Information);
                        LimpiarCamposPartido();
                        ActualizarTablaTorneosFin();
                        BotonesPartidoEdicion(false);
                    }
                    else
                    {
                        MessageBox.Show("No se pudo eliminar el Partido", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
        }

        private void btnPartidoEditar_Click(object sender, RoutedEventArgs e)
        {
            if (accionPartido == accion.Editar)
            {
                TorneosFin a = dtgtorneofin.SelectedItem as TorneosFin;
                a.EquipoUno = Equipo1.Text;
                a.EquipoDos = Equipo2.Text;
                if (int.Parse(Marcador1.Text) > int.Parse(Marcador2.Text))
                {
                    a.EquipoUnoMarcador = 3;
                    a.EquipoDosMarcador = 1;
                }
                if (int.Parse(Marcador1.Text) < int.Parse(Marcador2.Text))
                {
                    a.EquipoUnoMarcador = 1;
                    a.EquipoDosMarcador = 3;
                }
                if (int.Parse(Marcador1.Text) == int.Parse(Marcador2.Text))
                {
                    a.EquipoUnoMarcador = 2;
                    a.EquipoDosMarcador = 2;
                }
                ManejadorTorneoFin.Modificar(a);
            }
            LimpiarCamposPartido();
            ActualizarTablaTorneosFin();
            BotonesPartidoEdicion(false);
        }

        private void btnPartidoNuevo_Click_1(object sender, RoutedEventArgs e)
        {
            LimpiarCamposPartido();
            BotonesPartidoEdicion(true);
            accionPartido = accion.Nuevo;
        }

        private void AgregarATorneoImp()
        {
            SegundoEquipo();
            TercerEquipo();
            TorneosFin a = new TorneosFin()
            {
                Fecha = clcFachaTor.Text,
                EquipoUno = equipo2,
                EquipoDos = equipo1,
                EquipoDosMarcador = 0,
                EquipoUnoMarcador = 0,
            };
            ManejadorTorneoFin.Agregar(a);
        }
    }
}
