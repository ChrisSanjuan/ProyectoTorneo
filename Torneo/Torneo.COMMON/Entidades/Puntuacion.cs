﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Torneo.COMMON.Entidades
{
    public class Puntuacion : Base
    {
        public string TipoDeporte { get; set; }
        public string EquipoDeporte { get; set; }
        public string Historial { get; set; }
    }
}
