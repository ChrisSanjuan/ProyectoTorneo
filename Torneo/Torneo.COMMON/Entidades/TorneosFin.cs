﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Torneo.COMMON.Entidades
{
    public class TorneosFin:Base
    {
        public string Fecha { get; set; }
        public string EquipoUno { get; set; }
        public int EquipoUnoMarcador { get; set; }
        public string EquipoDos { get; set; }
        public int EquipoDosMarcador { get; set; }
    }
}
