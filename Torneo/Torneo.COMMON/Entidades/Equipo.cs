﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Torneo.COMMON.Entidades
{
    public class Equipo : Base
    {
        public string NombreEquipo { get; set; }
        public string TipoDeporte { get; set; }       
    }
}
