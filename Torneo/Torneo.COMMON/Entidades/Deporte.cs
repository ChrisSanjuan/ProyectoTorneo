﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Torneo.COMMON.Entidades
{
    public class Deporte : Base
    {
        public string NombreDeporte { get; set; }
        public override string ToString()
        {
            return NombreDeporte;
        }
    }
}
