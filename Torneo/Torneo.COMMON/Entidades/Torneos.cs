﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Torneo.COMMON.Entidades
{
    public class Torneos : Base
    {
        public DateTime FechaProgramada { get; set; }
        public string TipoTorneo { get; set; }
        public override string ToString()
        {
            return TipoTorneo;
        }
    }
}
