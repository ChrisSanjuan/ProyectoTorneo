﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Torneo.COMMON.Entidades;
using Torneo.COMMON.Interfaz;

namespace Torneo.BIZ
{
    public class ManejadorTorneoFin: IManejadorTorneoFin4
    {
        IRepositorio<TorneosFin> repositorio;
        public ManejadorTorneoFin(IRepositorio<TorneosFin> repo)
        {
            repositorio = repo;
        }
        public List<TorneosFin> Listar => repositorio.Leer;

        public bool Agregar(TorneosFin entidad)
        {
            return repositorio.Crear(entidad);
        }

        public TorneosFin BuscarId(string id)
        {
            return Listar.Where(e => e.Id == id).SingleOrDefault();
        }



        public bool Eliminar(string id)
        {
            return repositorio.Eliminar(id);
        }

        public bool Modificar(TorneosFin entidad)
        {
            return repositorio.Editar(entidad);
        }

    }
}
