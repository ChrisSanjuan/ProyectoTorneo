﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Torneo.COMMON.Entidades;
using Torneo.COMMON.Interfaz;

namespace Torneo.BIZ
{
    public class ManejadorPuntuacion : IManejadorPuntuacion
    {
        IRepositorio<Puntuacion> repositorio;
        public ManejadorPuntuacion(IRepositorio<Puntuacion> repo)
        {
            repositorio = repo;
        }
        public List<Puntuacion> Listar => repositorio.Leer;

        public bool Agregar(Puntuacion entidad)
        {
            return repositorio.Crear(entidad);
        }

        public Puntuacion BuscarId(string id)
        {
            return Listar.Where(e => e.Id == id).SingleOrDefault();
        }

        public bool Eliminar(string id)
        {
            return repositorio.Eliminar(id);
        }

        public bool Modificar(Puntuacion entidad)
        {
            return repositorio.Editar(entidad);
        }
    }
}
