﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Torneo.COMMON.Entidades;
using Torneo.COMMON.Interfaz;

namespace Torneo.BIZ
{
    public class ManejadorEquipo : IManejadorEquipo
    {
        IRepositorio<Equipo> repositorio;
        public ManejadorEquipo(IRepositorio<Equipo> repo)
        {
            repositorio = repo;
        }
        public List<Equipo> Listar => repositorio.Leer;

        public bool Agregar(Equipo entidad)
        {
            return repositorio.Crear(entidad);
        }

        public Equipo BuscarId(string id)
        {
            return Listar.Where(e => e.Id == id).SingleOrDefault();
        }

        public bool Eliminar(string id)
        {
            return repositorio.Eliminar(id);
        }

        public bool Modificar(Equipo entidad)
        {
            return repositorio.Editar(entidad);
        }
    }
}
