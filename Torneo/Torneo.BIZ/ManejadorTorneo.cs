﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Torneo.COMMON.Entidades;
using Torneo.COMMON.Interfaz;

namespace Torneo.BIZ
{
    public class ManejadorTorneo : IManejadorTorneo
    {
        IRepositorio<Torneos> repositorio;
        public ManejadorTorneo(IRepositorio<Torneos> repo)
        {
            repositorio = repo;
        }
        public List<Torneos> Listar => repositorio.Leer;

        public bool Agregar(Torneos entidad)
        {
            return repositorio.Crear(entidad);
        }

        public Torneos BuscarId(string id)
        {
            return Listar.Where(e => e.Id == id).SingleOrDefault();
        }

        public bool Eliminar(string id)
        {
            return repositorio.Eliminar(id);
        }

        public bool Modificar(Torneos entidad)
        {
            return repositorio.Editar(entidad);
        }
    }
}
