﻿using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Torneo.COMMON.Entidades;
using Torneo.COMMON.Interfaz;

namespace Torneo.DAL
{
    public class RepositorioPuntuacion : IRepositorio<Puntuacion>
    {
        private string DBName = "Inventario.db";
        private string TableName = "Puntos";
        public List<Puntuacion> Leer
        {
            get
            {
                List<Puntuacion> datos = new List<Puntuacion>();
                using (var db = new LiteDatabase(DBName))
                {
                    datos = db.GetCollection<Puntuacion>(TableName).FindAll().ToList();
                }
                return datos;
            }
        }

        public bool Crear(Puntuacion entidad)
        {
            entidad.Id = Guid.NewGuid().ToString();
            try
            {
                using (var db = new LiteDatabase(DBName))
                {
                    var coleccion = db.GetCollection<Puntuacion>(TableName);
                    coleccion.Insert(entidad);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Editar(Puntuacion entidadModificada)
        {
            try
            {
                using (var db = new LiteDatabase(DBName))
                {
                    var coleccion = db.GetCollection<Puntuacion>(TableName);
                    coleccion.Update(entidadModificada);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Eliminar(string id)
        {
            try
            {
                int r;
                using (var db = new LiteDatabase(DBName))
                {
                    var coleccion = db.GetCollection<Puntuacion>(TableName);
                    r = coleccion.Delete(e => e.Id == id);
                }
                return r > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
