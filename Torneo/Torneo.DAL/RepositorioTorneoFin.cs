﻿using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Torneo.COMMON.Entidades;
using Torneo.COMMON.Interfaz;

namespace Torneo.DAL
{

    public class RepositorioTorneoFin:IRepositorio<TorneosFin>
    {
        private string DBName = "Inventario.db";
        private string TableName = "TorneosFin";
        public List<TorneosFin> Leer
        {
            get
            {
                List<TorneosFin> datos = new List<TorneosFin>();
                using (var db = new LiteDatabase(DBName))
                {
                    datos = db.GetCollection<TorneosFin>(TableName).FindAll().ToList();
                }
                return datos;
            }
        }

        public bool Crear(TorneosFin entidad)
        {
            entidad.Id = Guid.NewGuid().ToString();
            try
            {
                using (var db = new LiteDatabase(DBName))
                {
                    var coleccion = db.GetCollection<TorneosFin>(TableName);
                    coleccion.Insert(entidad);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Editar(TorneosFin entidadModificada)
        {
            try
            {
                using (var db = new LiteDatabase(DBName))
                {
                    var coleccion = db.GetCollection<TorneosFin>(TableName);
                    coleccion.Update(entidadModificada);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Eliminar(string id)
        {
            try
            {
                int r;
                using (var db = new LiteDatabase(DBName))
                {
                    var coleccion = db.GetCollection<TorneosFin>(TableName);
                    r = coleccion.Delete(e => e.Id == id);
                }
                return r > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
